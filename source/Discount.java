
public class Discount {
	double discount;
	double cost;
	
	public Discount(double discount, double cost) {
		super();
		this.discount = discount;
		this.cost = afterDiscount(discount, cost);
	}
	
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}

	public double afterDiscount(double discount, double cost) {
		cost = cost * (1 - discount);
		return cost;
	}
}
