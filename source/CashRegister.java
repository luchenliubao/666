import java.util.ArrayList;
import java.util.Scanner;

public class CashRegister
{
   
   public static void main(String[] args)
   {
	      String s,change,tender,cost;
	      double balance,Tcost = 0;
	      ArrayList<String> nameList = new ArrayList<String>();
	      ArrayList<Double> costList = new ArrayList<Double>();
	      ArrayList<String> discountList = new ArrayList<String>();
	      ArrayList<String> afterdiscountList = new ArrayList<String>();

	      String prompt = "n",enterprompt,receiptprompt;
	      Scanner in = new Scanner(System.in);
	      
	      System.out.println("+--------------------------------------+");
	      System.out.println("|Welcome to use Sixsixsix cash register|");
	      System.out.println("+--------------------------------------+");
	      System.out.print("Please enter cash register's float: $");
          while (in.hasNextDouble()==false){
        	  System.out.println("Please enter the right cash register's float");
	          in.nextLine();
          }
	      s = in.nextLine();
	      balance = Double.parseDouble(s);
	      
	      while(prompt !="y")
	      {
	    	  double thiscost = 0;
	          Scanner ms = new Scanner(System.in);
	          System.out.println("Do you want to exit, y/n");
	          prompt = ms.nextLine().toString();
	          
	          Scanner enter = new Scanner(System.in);
	         
	    	  if(prompt.equalsIgnoreCase("y"))
	    	  {
	    		  System.out.println("Balance of the Cash Register: $"+Double.toString(balance));
	    		  System.exit(0);
	    	  }
	    	  else if(prompt.equalsIgnoreCase("n")){
		          do{System.out.print("Please enter the item's name:");
		          s = in.nextLine();
		          nameList.add(s);
		          
		          System.out.print("Please enter the item's cost: $");
		          cost = in.nextLine();
		          costList.add(Double.parseDouble(cost));
	          
		          System.out.print("Discount of this item: ");
		          s = in.nextLine();
		          Discount Discount = new Discount(Double.parseDouble(s),Double.parseDouble(cost));
		          cost = Double.toString(Discount.getCost());
		          discountList.add(s);
		          afterdiscountList.add(cost);
	          
		          thiscost += Discount.getCost();
		          Tcost += Discount.getCost();
		          
		          System.out.println("If all items have been entered, y/n");
		          enterprompt = enter.nextLine().toString();
		          }while(enterprompt.equals("n"));
		          
		          System.out.print("Please enter the cash amount tendered: $");
		          while (in.hasNextDouble()==false){
		        	  System.out.println("Please enter the right cash amount tendered");
			          in.nextLine();
		          }
		          s = in.nextLine();
		          tender = s;
		          
		          Transaction trans = new Transaction(s, thiscost);
		          change = Double.toString(Double.parseDouble(s) - trans.getCost());
		          System.out.println("Amount of change required = $" + change);

		          balance = balance + trans.getCost();
		          Scanner receipt = new Scanner(System.in);
		          System.out.println("Do you want to provide a receipt, y/n");
		          receiptprompt = receipt.nextLine().toString();
		          
		          while(! receiptprompt.equalsIgnoreCase("y") && ! receiptprompt.equalsIgnoreCase("n")){
			          System.out.println("Do you want to provide a receipt, y/n");
			          receiptprompt = receipt.nextLine().toString();
		          }
		          if(receiptprompt.equalsIgnoreCase("y")) {
		        	  System.out.println("name             cost         discount       afterDiscount");
		        	  System.out.println("-----------------------------------------------------------");
		        	  for(int i = 0;i < nameList.size();i++) {
			        	  System.out.println(nameList.get(i)+"             "+costList.get(i)+"             "
			        			  +discountList.get(i)+"             "+afterdiscountList.get(i));
		        	  }
		        	  System.out.println("-----------------------------------------------------------");
		        	  System.out.println("cash amount tendered:   $"+tender);
		        	  System.out.println("cash amount change:   $"+change);
		          }else {
		        	  System.exit(0);
		          }
		          
	    	  }
	    	  else{
		          System.out.println("Do you want to exit, y/n");
		          prompt = ms.nextLine().toString();	    		  
	    	  }

	          

	      }
   }
}
